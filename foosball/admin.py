# -*- coding:utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import Profile, Team, Tourney, Round


class UserInline(admin.StackedInline):
    model = Profile
    can_delete = False


# Определяем новый класс настроек для модели User
class UserAdmin(UserAdmin):
    inlines = (UserInline, )

class TourneyAdmin(admin.ModelAdmin):
    fields = ('name','tourney_user','form')
    list_display = ('name','form')


# Перерегистрируем модель User
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Team)
admin.site.register(Tourney, TourneyAdmin)
admin.site.register(Round)