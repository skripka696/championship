"""football URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from foosball import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [

    url(r'^$', views.Regist.as_view(), name = 'regist'),
    url(r'^get_save/$', views.SaveView.as_view(), name = 'get_save'),
    url(r'^login/$', views.Login.as_view(), name = 'login'),
    url(r'^logout/$', views.LogOut.as_view(), name = 'logout'),
    url(r'^personal_cabinet/(?P<pk>\d+)/$', views.GetUserId.as_view(), name = 'list'),
    # url(r'^turney/(?P<pk>\d+)/$', views.GetUserId.as_view(), name = 'list'),
    url(r'^user/$', views.UserView.as_view(), name = 'user'),
    url(r'^team/$', views.Team.as_view(), name = 'team'),
    url(r'^tourney/$', views.Tourney.as_view(), name = 'tourney'),
    url(r'^round/$', views.Round.as_view(), name = 'round'),
    url(r'^round_play_off_1_8/$', views.RoundPlayOff_1_8.as_view(), name = 'round_play_off_1_8'),
    url(r'^round_play_off_1_4/$', views.RoundPlayOff_1_4.as_view(), name = 'round_play_off_1_4'),
    url(r'^round_play_off_1_2/$', views.RoundPlayOff_1_2.as_view(), name = 'round_play_off_1_2'),
    url(r'^tournament_grid1/$', views.TournamentGrid.as_view()),
    url(r'^tournament_grid/$', views.TournamentGrid1.as_view(), name = 'tournament_grid1'),
    url(r'^tournament_table/$', views.TournamentTable.as_view(), name = 'tournament_table'),
    url(r'^finale/$', views.Finale.as_view(), name = 'finale'),
    url(r'^new_play/$', views.NewRound.as_view(), name = 'new_play'),
    url(r'^social/', include('social.apps.django_app.urls', namespace='social')),



]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)