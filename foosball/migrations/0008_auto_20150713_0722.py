# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('foosball', '0007_auto_20150713_0713'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='missed',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='team',
            name='scored',
            field=models.IntegerField(default=0),
        ),
    ]
