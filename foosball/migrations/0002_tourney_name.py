# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('foosball', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tourney',
            name='name',
            field=models.CharField(default=b'tourney', max_length=255),
        ),
    ]
