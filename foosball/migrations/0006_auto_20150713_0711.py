# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('foosball', '0005_auto_20150713_0630'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='team',
            name='team_user',
        ),
        migrations.AddField(
            model_name='team',
            name='team_user',
            field=models.ForeignKey(blank=True, to='foosball.Tourney', null=True),
        ),
    ]
