# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('avatar', models.ImageField(default=b'photo/index.jpeg', upload_to=b'photo/', verbose_name=b'photo')),
                ('order', models.DecimalField(default=0, max_digits=5, decimal_places=2)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Profile',
                'verbose_name_plural': 'Profiles',
            },
        ),
        migrations.CreateModel(
            name='Round',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('round_type', models.CharField(default=b'regular championship', max_length=255, choices=[(b'R_C', b'regular championship'), (b'play-off', ((b'1/8', b'1/8'), (b'1/4', b'1/4'), (b'1/2', b'1/2'), (b'finale', b'finale')))])),
            ],
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'name', max_length=255)),
                ('mark', models.IntegerField(default=0)),
                ('games', models.IntegerField(default=0)),
                ('win', models.IntegerField(default=0)),
                ('defeat', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Tourney',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('form', models.CharField(default=b'N_S', max_length=255, choices=[(b'F', b'finished'), (b'C', b'current'), (b'N_S', b'no_started')])),
                ('tourney_user', models.ManyToManyField(to=settings.AUTH_USER_MODEL, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='team',
            name='team_user',
            field=models.ManyToManyField(to='foosball.Tourney'),
        ),
        migrations.AddField(
            model_name='round',
            name='round_team',
            field=models.ManyToManyField(to='foosball.Team', blank=True),
        ),
    ]
