# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('foosball', '0004_auto_20150713_0603'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='team',
            name='team_user',
        ),
        migrations.AddField(
            model_name='team',
            name='team_user',
            field=models.ManyToManyField(to='foosball.Tourney', blank=True),
        ),
    ]
