from django import forms
from models import Round, Team


class RoundForm(forms.ModelForm):
	class Meta:
		model = Round
		fields = '__all__'

class TeamForm(forms.ModelForm):
	class Meta:
		model = Team
		fields = '__all__'







