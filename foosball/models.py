from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User)
    avatar = models.ImageField(upload_to='photo/',default="photo/index.jpeg", verbose_name='photo')
    order = models.DecimalField(default=0, max_digits=5, decimal_places=2)

    class Meta:
        verbose_name = 'Profile'
        verbose_name_plural = 'Profiles'

    def __unicode__(self):
        return '{0} {1} {2}'.format( self.user, self.avatar, self.order)


class Tourney(models.Model):
    name = models.CharField(max_length=255, default='tourney')
    tourney_user = models.ManyToManyField(User, blank=True)

    GENDER_CHOICES =(
        ('F', 'finished'),
        ('C', 'current'),
        ('N_S','no_started')
    )
    form = models.CharField(max_length=255, choices= GENDER_CHOICES, default='N_S')

    def __unicode__(self):
        return '{0} {1}'.format( self.form, self.tourney_user)


class Team(models.Model):
    name = models.CharField(max_length=255, default='name')
    team_user = models.ManyToManyField(User, blank=True)
    mark = models.IntegerField(default=0)
    games = models.IntegerField(default=0)
    win = models.IntegerField(default=0)
    defeat = models.IntegerField(default=0)
    scored = models.IntegerField(default=0)
    missed = models.IntegerField(default=0)
    place = models.IntegerField(default=0)

    def __unicode__(self):
        return '{0} {1}'.format( self.name, self.team_user)


class Round(models.Model):
    round_team = models.ManyToManyField(Team, blank=True)
    GENDER_CHOICES =(
        ('R_C', 'regular championship'),
        ('play-off',(
            ('1/8','1/8'),
            ('1/4', '1/4'),
            ('1/2', '1/2'),
            ('finale', 'finale')
        )),
    )
    round_type = models.CharField(max_length=255, choices=GENDER_CHOICES, default='regular championship')

    def __unicode__(self):
        return '{0} {1}'.format( self.round_team, self.round_type)



