from django.shortcuts import render
from django.shortcuts import render, render_to_response, redirect

from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.views.generic.edit import FormView, View, UpdateView, DeleteView, BaseUpdateView
from django.views.generic import TemplateView, DetailView, ListView
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import login, logout
from django.contrib.auth.models import User
from foosball.forms import RoundForm, TeamForm
from foosball.models import Profile, Tourney,Team,Round
from foosball.actions import team, tourney, team_table, regular, mark, finale, win, round_list, play_off_1_8, play_off_1_4, play_off_1_2, new_round, user


class SaveView(TemplateView):
    template_name = 'foosball/save.html'

# class List(TemplateView):
#     template_name = 'foosball/personal_cabinet.html'

class Regist(FormView):
    form_class = UserCreationForm
    success_url = '/get_save/'
    template_name = 'foosball/regist.html'

    def form_valid(self,form):
        self.object = form.save()
        return super(Regist, self).form_valid(form)


class Login(FormView):
    form_class = AuthenticationForm
    success_url = '/personal_cabinet/1/'
    template_name = 'foosball/login.html'

    def form_valid(self,form):
        user = form.get_user()
        login(self.request, user)
        return super(Login, self).form_valid(form)


class LogOut(View):
    success_url = '/login/'
    template_name = 'foosball/base.html'

    def get(self,request):
        logout(request)
        return HttpResponseRedirect('/login')


class GetUserId(UpdateView):
    model = User
    # template_name_suffix = 'personal_cabinet'
    template_name = 'foosball/personal_cabinet.html'
    success_url = '/personal_cabinet/1/'
    fields = ['first_name','last_name', 'email']

class UserView(TemplateView):
    template_name = 'foosball/user_for_team.html'

    def get_context_data(self, **kwargs):
        context = super(UserView, self).get_context_data(**kwargs)
        context['user'] = user()
        return context

class Team(ListView):
    model = Team
    # form_class = TeamForm
    template_name = 'foosball/team.html'

    def get_context_data(self, **kwargs):
        context = super(Team, self).get_context_data(**kwargs)
        context['team'] = team()
        return context

class Tourney(TemplateView):
    model = Tourney
    template_name = 'foosball/tourney.html'
    success_url = '/personal_cabinet/1/'


    def get_context_data(self, **kwargs):
        context = super(Tourney, self).get_context_data(**kwargs)
        context['tourney'] = tourney()
        return context

class Tourney(TemplateView):
    model = Tourney
    template_name = 'foosball/tourney.html'
    success_url = '/personal_cabinet/1/'


    def get_context_data(self, **kwargs):
        context = super(Tourney, self).get_context_data(**kwargs)
        context['tourney'] = tourney()
        return context

class Round(TemplateView):
    model = Round
    template_name = 'foosball/round.html'
    success_url = '/personal_cabinet/1/'

    # def get(self, request, *args, **kwargs):
    #     round = Round.objects.all()
    #     return render(request, self.template_name, {'tourney_list': tourney})

    def get_context_data(self, **kwargs):
        context = super(Round, self).get_context_data(**kwargs)

        context['round_list'] = round_list()
        context['regular'] = regular()

        return context


class RoundPlayOff_1_8(TemplateView):
    model = Round
    template_name = 'foosball/play_off_1_8.html'
    success_url = '/personal_cabinet/1/'

    def get_context_data(self, **kwargs):
        context = super(RoundPlayOff_1_8, self).get_context_data(**kwargs)
        context['play_off_1_8'] = play_off_1_8()
        return context


class RoundPlayOff_1_4(TemplateView):
    model = Round
    template_name = 'foosball/play_off_1_4.html'
    success_url = '/personal_cabinet/1/'

    def get_context_data(self, **kwargs):
        context = super(RoundPlayOff_1_4, self).get_context_data(**kwargs)
        context['play_off_1_4'] = play_off_1_4()
        return context


class RoundPlayOff_1_2(TemplateView):
    model = Round
    template_name = 'foosball/play_off_1_2.html'
    success_url = '/personal_cabinet/1/'

    def get_context_data(self, **kwargs):
        context = super(RoundPlayOff_1_2, self).get_context_data(**kwargs)
        context['play_off_1_2'] = play_off_1_2()
        return context

class Finale(TemplateView):
    model = Team
    template_name = 'foosball/finale.html'
    success_url = '/personal_cabinet/1/'

    def get_context_data(self, **kwargs):
        context = super(Finale, self).get_context_data(**kwargs)
        context['finale'] = finale()
        context['win'] = win()

        return context


class TournamentGrid(TemplateView):
    template_name = 'foosball/tournament_grid.html'
    success_url = '/personal_cabinet/1/'

    def get_context_data(self, **kwargs):
        context = super(TournamentGrid, self).get_context_data(**kwargs)
        context['regular'] = regular()
        context['mark'] = mark()
        context['play_off_1_8'] = play_off_1_8()
        context['play_off_1_4'] = play_off_1_4()
        context['play_off_1_2'] = play_off_1_2()
        context['finale'] = finale()
        context['win'] = win()
        context['tourney'] = tourney()
        return context


class TournamentGrid1(TemplateView):
    template_name = 'foosball/tournament_grid1.html'
    success_url = '/personal_cabinet/1/'

    def get_context_data(self, **kwargs):
        context = super(TournamentGrid1, self).get_context_data(**kwargs)
        context['regular'] = regular()
        context['mark'] = mark()
        context['play_off_1_8'] = play_off_1_8()
        context['play_off_1_4'] = play_off_1_4()
        context['play_off_1_2'] = play_off_1_2()
        context['finale'] = finale()
        context['win'] = win()
        context['tourney'] = tourney()
        return context

class TournamentTable(TemplateView):
    template_name = 'foosball/tournament_table.html'
    success_url = '/personal_cabinet/1/'

    def get_context_data(self, **kwargs):
        context = super(TournamentTable, self).get_context_data(**kwargs)
        context['team_table'] = team_table()

        return context

class NewRound(FormView):
    model = Round
    form_class = RoundForm
    template_name = 'foosball/new_play.html'

    def get_context_data(self, *args, **kwargs):
        context = super(NewRound, self).get_context_data(*args, **kwargs)
        context['new_round'] = new_round()
        return context







